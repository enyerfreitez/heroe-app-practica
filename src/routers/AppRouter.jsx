import { Routes, Route, BrowserRouter } from 'react-router-dom';

import LoginScreen from '../components/login/LoginScreen';

import DashboardRouter from './DashboardRouter';

const AppRouter = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/login' element={<LoginScreen />} />
				<Route path='/dashboard/*' element={<DashboardRouter />} />
			</Routes>
		</BrowserRouter>
	);
};

export default AppRouter;
