import { Routes, Route } from 'react-router-dom';
import Navbar from '../components/ui/Navbar';
import DcScreen from '../components/dc/DcScreen';
import MarverScreen from '../components/marvel/MarvelScreen';
import SearchScreen from '../components/search/SearchScreen';
import HeroScreen from '../components/hero/HeroScreen';

const DashboardRouter = () => {
	return (
		<>
			<Navbar />
			<div className='container'>
				<Routes>
					<Route path='dc' element={<DcScreen />} />
					<Route path='marvel' element={<MarverScreen />} />

					<Route path='search' element={<SearchScreen />} />
					<Route path='hero' element={<HeroScreen />} />
				</Routes>
			</div>
		</>
	);
};

export default DashboardRouter;
