import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';

const Navbar = () => {
	const [url, setUrl] = useState('');

	useEffect(() => {
		if (url === '') return;
		document.title = url;
	});

	return (
		<nav className='navbar navbar-expand-sm navbar-dark bg-dark'>
			<Link
				className='navbar-brand'
				to='/'
				onClick={() => setUrl('Asociaciones')}
			>
				Asociaciones
			</Link>

			<div className='navbar-collapse'>
				<div className='navbar-nav'>
					<NavLink
						className={({ isActive }) =>
							'nav-item nav-link ' + (isActive ? 'active' : '')
						}
						to='marvel'
						onClick={() => setUrl('Marvel')}
					>
						Marvel
					</NavLink>

					<NavLink
						className={({ isActive }) =>
							'nav-item nav-link ' + (isActive ? 'active' : '')
						}
						to='dc'
						onClick={() => setUrl('DC')}
					>
						DC
					</NavLink>

					<NavLink
						className={({ isActive }) =>
							'nav-item nav-link ' + (isActive ? 'active' : '')
						}
						to='search'
						onClick={() => setUrl('Search')}
					>
						Search
					</NavLink>

					<NavLink
						className={({ isActive }) =>
							'nav-item nav-link ' + (isActive ? 'active' : '')
						}
						to='hero'
						onClick={() => setUrl('Hero')}
					>
						Hero
					</NavLink>
				</div>
			</div>

			<div className='navbar-collapse collapse w-100 order-3 dual-collapse2 d-flex justify-content-end'>
				<ul className='navbar-nav ml-auto'>
					<span className='nav-item nav-link text-info'>Enyer</span>

					<button className='nav-item nav-link btn' /*onClick={handleLogout}*/>
						Logout
					</button>
				</ul>
			</div>
		</nav>
	);
};

export default Navbar;
