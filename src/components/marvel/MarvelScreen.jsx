import HeroList from '../hero/HeroList';

const MarverScreen = () => {
	return (
		<div>
			<h1>MarverScreen</h1>
			<hr />
			<HeroList publisher='Marvel Comics' />
		</div>
	);
};

export default MarverScreen;
