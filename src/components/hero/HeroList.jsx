import PropTypes from 'prop-types';

import HeroCard from './HeroCard';

import getHeroesByPublisher from '../../helpers/getHeroesByPublisher';

const HeroList = ({ publisher }) => {
	const heroes = getHeroesByPublisher(publisher);
	return (
		<div className='row row-cols-1 row-cols-md-3 g-3'>
			{heroes.map((hero) => (
				<HeroCard key={hero.id} {...hero} />
			))}
		</div>
	);
};

HeroList.propTypes = {
	publisher: PropTypes.string.isRequired,
};

export default HeroList;
