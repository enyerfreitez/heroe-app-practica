import heroes from '../data/heroes';

const getHeroesByPublisher = (publisher) => {
	const dataHeroes = ['Marvel Comics', 'DC Comics'];

	if (!dataHeroes.includes(publisher))
		throw new Error(`${publisher} is not Valid`);

	return heroes.filter((hero) => hero.publisher === publisher);
};

export default getHeroesByPublisher;
